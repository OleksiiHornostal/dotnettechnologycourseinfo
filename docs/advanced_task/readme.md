## ADVANCED task ##

You need to do this task if you did not do any lab and want 5A mark or you did some 1st labs but not all. So in any case when you want a good mark and you have some troubles with labs you can implement the program that would be described in this advanced task.

### Task Description ###

You need to create a Console Game with Visul Studio 2019 Community Edition and C# programming language. What should be in each game:

1. Main Menu where user can see avalilable commands such as: create new game, open saved games, settings, exit.
2. Craete New Game feature that start new game.
3. Save And Resore Game features that can save game data to the disc (to the file) and restore it from the disc (from the file).
4. Full Game Logic.

### Which Games you can create? ###

* [The Matchstick Game](https://math.stackexchange.com/questions/180333/winning-strategy-for-a-matchstick-game)
* [The Tic-tac-toe Game](https://en.wikipedia.org/wiki/Tic-tac-toe)
* [The Anagram Game](https://en.wikipedia.org/wiki/Anagram): you need to have a dictionary of words and display jumbled letters, after which the user must guess what the word is
* [The Snake Game](https://en.wikipedia.org/wiki/Snake_(video_game_genre))
* [The Sudoku Game](https://sudoku.com/how-to-play/sudoku-rules-for-complete-beginners/)

**!!! To pass this task you need to crate a Web Application with all implemented steps and you need to create a report with screenshots and descriptions of them. **

### What should be in report? ###

* A screenshot of the Main Menu.
* A screenshot of each command (submenu).
* Screenshots of the game states (geme just started, you are playing the game, game was successfully and unseccsfully finished).

### Useful links ###

* [What is it ASP NET MVC?](https://dotnet.microsoft.com/apps/aspnet/mvc)
* [How to create your first ASP.NET MVC Web Application with Visual Studion and C#](https://docs.microsoft.com/en-us/aspnet/mvc/overview/getting-started/introduction/getting-started)
* [How to display data in ASP.NET MVC Web Application (please use the first method - Using foreach loop)](https://www.c-sharpcorner.com/UploadFile/f82e9a/data-displaying-in-table-format-in-diffrent-ways-in-mvc/)

#### Good luck! ####