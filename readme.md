# Dot Net Technology Course 2023 #

My name is **Oleksii Hornostal**. I'm assistant professor at the Department of Computer Engineering and Programming in National Technical University "Kharkiv Polytechnic Institute". I'm your Lecturer in .NET Software Technology Course. I also work as .Net Software Engineer in Game Dev IT-company.
My contact email: oleksii.hornostal@khpi.edu.ua

This **README** would explain you everthing about this coursre and our working plans.

## What do you need to do to start? ##
* Create a bitbucket account and create a repository 
* Add me in Repository settings (users and permissions tab) to the repository as Admin - gornostalaa@gmail.com
* Create 2 folders in your repository: src and docs. You will upload you labs in src folder and you reports in docs folder. 
* Download and install MS Visual Studio Community Edition 2019 and add .Net Framework + .Net Core packages during installation process.
* [Fill a row in Google Spreadsheet (First/Last Names and Bitbucket url)](https://docs.google.com/spreadsheets/d/1m49vIDhULDrJQtkcOr9Jz5jMKG_jXgmBIKjAwbEKC9o/edit?usp=sharing  "Spreadsheet link")
* [Download this book and read all info related to the lab before each lab](https://drive.google.com/file/d/0Byvt49EOT9n3SnRRVm9WM2pTR2c/view?usp=sharing)

## Folders and files naming rules ##
* All C# projects for your labs should go to the src folder as DotNet_LastName_01
* All reports should be formatted as docx (Win Office) document as DotNet_LastName_01.docs (or you can use OpenOffice / LibreOffice)
* [Lab Report example](https://drive.google.com/file/d/1CmG1u4n31LJ_3ON0mDTBPIFdLYIloIW1/view?usp=sharing)

## Labs' tasks ##

We will have 16 study weeks and 2 modules with 5 tasks.

### MODULE 1 ###

1. [Development of programs in C#. Console applications](https://bitbucket.org/OleksiiHornostal/dotnettechnologycourseinfo/src/master/docs/lab01/)
2. [Collections of objects in C#. Formatted output](https://bitbucket.org/OleksiiHornostal/dotnettechnologycourseinfo/src/master/docs/lab02/)
3. [Processing collections of objects. Working with files](https://bitbucket.org/OleksiiHornostal/dotnettechnologycourseinfo/src/master/docs/lab03/)
4. [Class properties. String processing. StringBuilder](https://bitbucket.org/OleksiiHornostal/dotnettechnologycourseinfo/src/master/docs/lab04/)

### MODULE 2 ###

1. [Introduction to ASP.NET technology: creating web-sites with .NET and C#](https://bitbucket.org/OleksiiHornostal/dotnettechnologycourseinfo/src/master/docs/lab05/)

### ADVANCED TASK ###

To get 5A and 100 points you can do ONLY [this ADVANCED task](https://bitbucket.org/OleksiiHornostal/dotnettechnologycourseinfo/src/master/docs/advanced_task/).

## Course WorkFlow description ##

Every week I will provide you theory and tasks via links above. You need to read theory carefully, do your homework, upload it to the bitbucket repository and create a report whis also should be uploaded to the bitbucket.

###### Good luck! ######