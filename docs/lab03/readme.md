## Lab #3. Processing collections of objects. Working with files ##
### Theory block ###

To work with File Systems in C# (with files, directories etc) you need to add **using System.IO;**. Then you need to use these common dunctions:

1. **System.IO.File.WriteAllLines(fileName, linesOfData)** - creates a new file *fileName*, writes one or more strings to the file (string array) *linesOfData* and then closes the file. 
2. **System.IO.File.ReadAllLines(fileName)** - opens a text file *fileName*, reads all lines of the file into a string array (returning value) and then closes the file.

Also there are a lot of other functions to work with files like **System.IO.File.WriteAllText(fileName, text)** (writes some string *text* to the file *fileName*), **System.IO.File.ReadAllText(fileName)**  reads all lines of the file into the 1 long string.

### Home Task ###

You need to implement these logic in your console program:

* Saving the entered data in a file for later editing.
* Recovering previously saved data from a file.
* Searching and editing student data.
* Deleting of student personal data.
* In the report, provide a description of the methods and algorithms used for working with files.

**!!! To pass this task you need to crate a program with all implemented steps and you need to create a report with screenshots and descriptions of them. **

### Useful links ###

* [File and Stream I/O](https://docs.microsoft.com/en-us/dotnet/standard/io/?redirectedfrom=MSDN "File and Stream I/O")
* [How to write to a text file](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/file-system/how-to-write-to-a-text-file "How to write to a text file")
* [How to read from a text file](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/file-system/how-to-read-from-a-text-file "How to read from a text file")
* [How to read a text file one line at a time](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/file-system/how-to-read-a-text-file-one-line-at-a-time "How to read a text file one line at a time")

#### Good luck! ####
