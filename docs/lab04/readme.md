## Lab #4. Class properties. String processing. StringBuilder ##
### Theory block ###

To create read-only property in class you need to remove set; method and add some logic to the get;

```
using System;

public class Student 
{
	public DateTime BirthDate {get; set;}

    public int Age 
	{ 
		get 
		{ 
			var age = DateTime.Today.Year - BirthDate.Year;
			if (BirthDate.Date > DateTime.Today.AddYears(-age)) age--;
			return age; 
		}
	}
}
```

To work with date and time in C# you need to use DateTime class, its constructor and its method DateTime.Parse to transform string value DateTime object. To get day you can use Day property of DateTime object, to get month you can use Month property and for year - Year property.

Example:

```
//assigns year, month, day, hour, min, seconds, UTC timezone
DateTime date = new DateTime(2015, 12, 31, 5, 10, 20, DateTimeKind.Utc);

Console.Write("Enter a date (e.g. 10/22/1987): ");
DateTime inputtedDate = DateTime.Parse(Console.ReadLine());

Console.WriteLine(inputtedDate.Day);
Console.WriteLine(inputtedDate.Month);
Console.WriteLine(inputtedDate.Year);
```

To process string with SringBuilde you need to add **using System.Text;** to the top, create **StringBuilder** object and use **Append or AppendLine** method to add new string. Than use **ToString** method to get full string

Example:

```
var sb = new StringBuilder();
sb.AppendLine("str1");
sb.AppendLine("str2");
sb.AppendLine("str3");
Console.WriteLine(sb.ToString());
```

### Home Task ###

You need to implement these logic in your console program:

* Create a new ExtendedStudent class, which must be inherited from the Student class, adding the following fields to it: full date of birth, full day of admission (for example, September 1, 2018), faculty name, specialty number.
* Create class properties GetCourseNo and GetSemesterNo, which should only have a get modifier, in which you need to implement the logic for calculating the current course and semester based on the date of receipt and taking into account the fact that semesters begin on September 1 and February 1, respectively.
* Create a property of the GetGroupName class, which should contain only the get modifier with the implementation of the procedure for combining the faculty abbreviation, special number, year of receipt, etc. according to the current rules for the formation of group names with the subsequent return of such a string.
* Create a property of the GetCurrentAge class with a single get modifier and an implementation in it of the procedure for calculating the current age of a student based on his date of birth.
* Add to the generated class an implementation of the ToString method with all the new fields and using the StringBuilder class to process strings.

**!!! To pass this task you need to crate a program with all implemented steps and you need to create a report with screenshots and descriptions of them. **

### Useful links ###

* [How to use get and set for properties in C#](https://www.tutlane.com/tutorial/csharp/csharp-properties-get-set)
* [Working with dates in C#](https://www.tutorialsteacher.com/csharp/csharp-datetime)
* [StringBuilder Tutorial](https://www.tutorialsteacher.com/csharp/csharp-stringbuilder)

#### Good luck! ####