## Lab #2. Collections of objects in C#. Formatted output ##
### Theory block ###

To input integer from console you need to use **int.Parse(Console.ReadLine())** to get input data and transform it to the int type.

Example:

```
int studentsAmount = int.Parse(Console.ReadLine());
```

To create a list of objects you need to add **using System.Collection.Generic;** at the top of you .cs file and use **new List<TYPE>()** class and replace TYPE with you type name.

Example:

```
var students = new List<Student>();
```

To use for loop you need to put for keyword and then in round brackets put 3 expressions:

1. Create new counter variable and assign its initial value (usualy 0)
2. Add condition when loop should stop (ex. i < array.Length)
3. Update value of counter (usualy increment, ex. i++)

Example:

```
for(int i = 0; i < studentsAmount; i++)
{
 // your code goes here
}
```

To use foreach loop you need to put foreach keyword and then in round brackets create loop variable put *in* keywoard and put the the collecion name.

Example:

```
foreach(var student in students)
{
 // your code goes here
}
```

### Home Task ###

You need to implement these logic in your console program:

* Creating the list of Student class object (use class from the 1st lab).
* Input from the user the amount of students in the list.
* Use **for loop** to input all Students from console
* Add ToString overriding for the 1st lab Student class to get Student info string like this: "FirstName: Oleksii; LastName: Hornostal; Email: gornostalaa@gmail.com; PhoneNumber: +380666666666"
* Use **foreach loop** and ToStreang method for each Student object to output data to the console

**!!! To pass this task you need to crate a program with all implemented steps and you need to create a report with screenshots and descriptions of them. **

### Useful links ###

* [How to use List<T\> in C#](https://www.tutorialsteacher.com/csharp/csharp-list)
* [for loop Tutorial](https://www.programiz.com/csharp-programming/for-loop)
* [foreach loop Tutorial](https://www.programiz.com/csharp-programming/foreach-loop)

#### Good luck! ####