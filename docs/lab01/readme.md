## Lab #1. Development of programs in C#. Console applications ##
### Theory block ###

To create class in C# we are using **class** keyword. To create a property of the class you need to specify its **acces modificator** (private|public|protected|internal|protected internal|private protected) then **datatype** (string|int|bool|long|char etc) then **propery name** and **{get; set; } accessors**.

Example:

```
public class Student 
{
    public string FirstName {get; set;}
}
```

To create the new object of the class you need to create variable for it and then call constuctor with **new keyword**.

Example:

```
var student = new Student();
```

To input data in you program you need to use **Console.ReadLine()** function and store its return value.

Example:

```
student.FirstName = Console.ReadLine();
```

To output data in you program you need to use **Console.WriteLine( "StringDataToOutput" )** function and store its return value.

Example:

```
Console.WriteLine(student.FirstName);
```

### Home Task ###

You need to implement these logic in your console program:

* Create program with class Student with these fields: FirstName, LastName, PhoneNumber and Email.
* In Main function create the Student class object.
* Input data from the console to this object and output all data to the console back

**!!! To pass this task you need to crate a program with all implemented steps and you need to create a report with screenshots and descriptions of them. **

### Useful links ###

* [How to create your first C# Program](https://www.tutorialsteacher.com/csharp/first-csharp-program)
* [Consol Input / Outut Tutorial](https://www.programiz.com/csharp-programming/basic-input-output)

#### Good luck! ####
