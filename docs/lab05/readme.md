## Lab #5. Introduction to ASP.NET technology: creating web-sites with .NET and C# ##
### Theory block ###

ASP.NET MVC is a framework for building websites and web applications by implementing the MVC pattern.

The concept of the MVC (model - view - controller) pattern (template) assumes the division of the application into three components:

1. **The Controller** is a class that provides communication between user and system, view and data storage. It receives user input and processes it. And depending on the processing results, it sends a certain output to the user, for example, in the form of a view.
2. **The View** is the actual visual part or user interface of the application. Typically, the html page that the user sees when they visit the site.
3. **The Model** is a class that describes the logic of the data used.

To develop ASP NET MVC Applications you need to install **Visual Studio 2019 Community Edition** and **ASP.NET and web development toolkit** during Visual studio installation.

### Home Task ###

You need to create an ASP NET MVC 5 Web Application with default Visual Studio Template and then add the folowing things:

* Change the content of the Main Page (Home page) and display FullName and Group name on that page.
* Create Students page with Controller and Index Page for displaying Students and addd link to the top menu.
* Create .cshtml page with table for StudentsContrlooler Index page with studetns info (you need to use *foreach loop* and *table* tag).
* Implement logic to get Students from text file and pass them to the web page in studentsController using logic that was created in Lab04 (reading students form file).

**!!! To pass this task you need to crate a Web Application with all implemented steps and you need to create a report with screenshots and descriptions of them. **

### What should be in report? ###

* A screenshot of the project strcture with all exppanded folders (you need to show all pages and controllers).
* A screenshot of the Home page with you Full Name and Group No.
* A screenshot of the data file with Students (from lab04).
* A screenshot of the Stuents page (StudentsController Index Action) with the table that displays students from the text file.

### Useful links ###

* [What is it ASP NET MVC?](https://dotnet.microsoft.com/apps/aspnet/mvc)
* [How to create your first ASP.NET MVC Web Application with Visual Studion and C#](https://docs.microsoft.com/en-us/aspnet/mvc/overview/getting-started/introduction/getting-started)
* [How to display data in ASP.NET MVC Web Application (please use the first method - Using foreach loop)](https://www.c-sharpcorner.com/UploadFile/f82e9a/data-displaying-in-table-format-in-diffrent-ways-in-mvc/)

#### Good luck! ####